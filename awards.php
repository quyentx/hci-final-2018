<?php
?>

<!DOCTYPE html>
<html wtx-context="045DA9B1-E45A-4C02-8891-CFDB0CA6DE43" lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <title>Awards | Ruya Digital</title>
</head>


<body class="popup-capsule-opened" style="overflow: auto;">
<section class="section-home" style="display: none;">
    <div id="three-container" class="three-wp" style="opacity: 1;">
        <canvas width="1366" height="635" style="width: 1366px; height: 635px;"></canvas>
    </div>
</section>

<?php
include('includes/child_topbar.php');
?>

<?php
include('includes/child_bottombar.php');
?>
<div class="main-title-container childpage-opened" style="z-index: 7; display: block;">
    <p class="font-liquido-h1 font-color-black" style="top: 0px; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); font-size: 22vw;">Awards</p>
</div>
<div class="child-elements popup active" style="z-index: 6; position: relative; right: auto; bottom: auto; height: 100%;">
    <div class="child-element-content" style="height: auto;">
        <div class="popup-content-work-container" style="height: 100%;">
            <div class="child-body-content not-centered">
                <div class="child-text-container">
                    <div class="child-text">
                        <div><p class="center" style=" font-size: 70px; font-family: liquido;text-align: center; font-weight: bold">We love awards, especially if we get them.</p></div>
                        <div class="row center" style="font-family: liquido;">
                            <div class="column">
                                <p style=" font-size: 50px; text-align: right; font-weight: bold">2018</p>
                            </div>
                            <div class="column" style="font-family: Arial; width: 700px">
                                <p style="font-size:45px">
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                </p>
                            </div>

                            <div class="column">
                                <p style=" font-size: 50px; text-align: right; font-weight: bold">2017</p>
                            </div>
                            <div class="column" style="font-family: Arial; width: 700px">
                                <p style="font-size:45px">
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                </p>
                            </div>

                            <div class="column">
                                <p style=" font-size: 50px; text-align: right; font-weight: bold">2016</p>
                            </div>
                            <div class="column" style="font-family: Arial; width: 700px">
                                <p style="font-size:45px">
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                    Lorem ipsum dolor sit amet<br>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                include('includes/wannachat.php');
                ?>

                <?php
                include('includes/child_page_footer.php');
                ?>
            </div>
        </div>
    </div>
</div>
<footer></footer>
<script async="" src="media/audio.js"></script>
<script async="" src="media/all.js"></script>
</body>
</html>
