<?php
session_start();

if(isset($_POST['submit'])) {
	include_once('../includes/connection.php');

/*	$sql = "SELECT id,username,password FROM members where username = '$username' LIMIT 1";
	$query = mysqli_query($db, $sql);
	if($query) {
		$row = mysqli_fetch_row($query);
		$userId= $row[0];
		$dbUserName = $row[1];
		$dbPassword = $row[2];
	}
	if($username == $dbUserName && $password == $dbPassword) {
		$_SESSION['username'] = $username;
		$_SESSION['id'] = $userId;
		header('Location: cms.php');
	}
	else {
		echo "<b><i>Incorrect credentials</i><b>";
	}*/

    if(isset($_POST['username'], $_POST['password'])){
        $username = strip_tags($_POST['username']);
        $password = strip_tags($_POST['password']);

        if(empty($username) or empty($password)){
            $error = "Incorrect credentials";
        }else{
            $query = $pdo->prepare("SELECT id,username,password FROM members WHERE username = ? AND password = ?");
            $query->bindValue(1, $username);
            $query->bindValue(2, $password);
            $query->execute();

            $num = $query->rowCount();

            if($num == 1){
                $_SESSION['logged_in'] = true;
                header('Location: cms.php');
                exit();
            }else{
                $error = "Incorrect credentials";
            }
        }
    }

}
?>


<!DOCTYPE html>
<html>
<head>
	<title>Login Page</title>
</head>
<body>
<h1 style="text-align: center">Login</h1>
<?php
if(isset($error)){
    echo '<div style="color:#FF0000;text-align:center;font-size:17px;">'.$error.'</div>';
}
?>
<br>
<form method="post" action="index.php" autocomplete="off" style="text-align: center">
	<input type= "text" name = "username" placeholder="Enter username">
	<input type="password" name="password" placeholder="Enter password">
	<input type="submit" name="submit" value="Login">
</form>
</body>
</body>
</html>