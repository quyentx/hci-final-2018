<!DOCTYPE html>
<html>
<head>
    <title>Content Management System</title>
    <link rel="stylesheet" href="admin-styles.css">
</head>
<body>

<div class = "container">
    <a class="container title"  href="index.php" id ="logo"> Content Management System </a>
    <ol>
        <li><a class = "cms-item" href="update_home.php">Update home page</a></li>
        <li><a class = "cms-item" href="add_menu.php">Add menu item</a></li>
    </ol>
</div>

</body>
</html>