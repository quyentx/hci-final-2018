<?php
session_start();
include_once('../includes/connection.php');

if(isset($_POST['id'], $_POST['title'], $_POST['link'], $_POST['content'], $_POST['submit'])){
    $id = strip_tags($_POST['id']);
    $title = strip_tags($_POST['title']);
    $link = strip_tags($_POST['link']);
    $content = strip_tags($_POST['content']);

    if(empty($id)){
        $error = 'ID is required!';
    }else{
        $query = $pdo-> prepare('UPDATE awards SET title=?, link=?, content=? WHERE id=?');
        $query->bindValue(1, $title, PDO::PARAM_STR);
        $query->bindValue(2, $link, PDO::PARAM_STR);
        $query->bindValue(3, $content, PDO::PARAM_STR);
        $query->bindValue(4, $id, PDO::PARAM_INT);

        $query->execute();
        $success = "Field updated successfully!";
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Content Management System</title>
    <link rel="stylesheet" href="admin-styles.css">
</head>
<body>
<div style="text-align: left; padding-top: 30px; padding-left:20px">
<h1>Home page highlight</h1>
<?php
if(isset($error)){
    echo '<div style="color:#FF0000;text-align:left;font-size:17px;">'.$error.'</div>';
} elseif (isset($success)){
    echo '<div style="color:green;text-align:left;font-size:17px;">'.$success.'</div>';
}
?>
<br>
<form style="text-align: left; padding-left: 30px" id="homepage" method="post" action="update_home.php" autocomplete="off">
    <select name="id">
        <option id="1" value=1>1</option>
        <option id="2" value=2>2</option>
        <option id="3" value=3>3</option>
        <option id="4" value=4>4</option>
    </select><br> <br>
    <input type= "text" name = "title" placeholder="Award title"><br> <br>
    <input type= "text" name = "link" placeholder="Link to award nominee"><br><br>
    <textarea title="svgContent" rows="4" cols="50" name="content" form="homepage" placeholder="SVG image content"></textarea> <br> <br>
    <input type="submit" name="submit" value="Update">
</form>
</div>
</body>
</html>