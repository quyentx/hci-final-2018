<?php
session_start();
include_once('../includes/connection.php');

if(isset($_POST['menu-id'], $_POST['item-name'], $_POST['menu-link'], $_POST['submit'])){
    $menuid  = strip_tags($_POST['menu-id']);
    $name = strip_tags($_POST['item-name']);
    $menulink = strip_tags($_POST['menu-link']);

    if(empty($name) or (empty($menulink))){
        $error = ' All fields are required!';
    }else{
        $query = $pdo-> prepare('INSERT INTO menu-items VALUES (?, ?, ?);');
        $query->bindValue(1, $menuid, PDO::PARAM_INT);
        $query->bindValue(2, $name, PDO::PARAM_STR);
        $query->bindValue(3, $menulink, PDO::PARAM_STR);
        $query->execute();
        $success = "Menu item added successfully!";
    }
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>Content Management System</title>
    <link rel="stylesheet" href="admin-styles.css">
</head>
<body>
<div style="text-align: left; padding-top: 30px; padding-left:20px">
    <h1>Add new menu item</h1>
    <?php
    if(isset($error)){
        echo '<div style="color:#FF0000;text-align:left;font-size:17px;">'.$error.'</div>';
    } elseif (isset($success)){
        echo '<div style="color:green;text-align:left;font-size:17px;">'.$success.'</div>';
    }
    ?>
    <br>
    <form style="text-align: left; padding-left: 30px" id="menupage" method="post" action="add_menu.php" autocomplete="off">
        <input type= "number" name = "menu-id" placeholder="Id"><br> <br>
        <input type= "text" name = "item-name" placeholder="Item name"><br> <br>
        <input type= "text" name = "menu-link" placeholder="Link to content page"><br><br>
        <input type="submit" name="submit" value="Add">
    </form>
</div>
</body>
</html>
