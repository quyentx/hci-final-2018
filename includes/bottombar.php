<?php

?>

<!DOCTYPE html>
<html lang="en">
<body>
<div class="bottom-bar content-animable" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
    <div class="bottom-row">
        <div class="change-color-container"><p class="menu-font font-color-white color-text"
                                               style="opacity: 0; transform: matrix(1, 0, 0, 1, -34.325, -60);">Touch me</p>
        </div>
    </div>
    <div class="sound-container cursor-customized ">
        <div class="svg-container">
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                 viewBox="0 0 477.216 477.216" style="enable-background:new 0 0 477.216 477.216;" xml:space="preserve"> <g>
                    <path d="M453.858,105.116v-91.6c0-4.3-2.1-8.4-5.5-10.9c-3.5-2.5-8-3.3-12.1-2l-272.9,86.7c-5.6,1.8-9.4,7-9.4,12.9v91.7v0.1v175.3 c-14.3-9.9-32.6-15.3-51.8-15.3c-20.3,0-39.6,6.1-54.3,17.1c-15.8,11.9-24.5,28-24.5,45.5s8.7,33.6,24.5,45.5 c14.7,11,33.9,17.1,54.3,17.1s39.6-6.1,54.3-17.1c15.8-11.9,24.5-28,24.5-45.5v-212.8l245.9-78.2v156.6 c-14.3-9.9-32.6-15.3-51.8-15.3c-20.3,0-39.6,6.1-54.3,17.1c-15.8,11.9-24.5,28-24.5,45.5s8.7,33.6,24.5,45.5 c14.7,11,33.9,17.1,54.3,17.1s39.6-6.1,54.3-17.1c15.8-11.9,24.5-28,24.5-45.5v-222.3 C453.858,105.116,453.858,105.116,453.858,105.116z M102.158,450.216c-28.1,0-51.8-16.3-51.8-35.6c0-19.3,23.7-35.6,51.8-35.6 s51.8,16.3,51.8,35.6C153.958,434.016,130.258,450.216,102.158,450.216z M180.958,173.416v-63.4l245.9-78.1v63.4L180.958,173.416z M375.158,363.116c-28.1,0-51.8-16.3-51.8-35.6c0-19.3,23.7-35.6,51.8-35.6s51.8,16.3,51.8,35.6 C426.858,346.816,403.158,363.116,375.158,363.116z"></path>
                </g> </svg>
            <div class="stud"></div>
        </div>
        <p class="menu-font "><span class="music-phrase gray-font">Music: </span>
            <span class="value gray-font"> ON</span>
        </p>
    </div>
    <div class="contact-form-container js-open-popup cursor-customized ">
        <p class="menu-font gray-font ">Let's talk</p>
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
             viewBox="0 0 473 473" style="enable-background:new 0 0 473 473;" xml:space="preserve"> <g>
                <g>
                    <path d="M403.581,69.3c-44.7-44.7-104-69.3-167.2-69.3s-122.5,24.6-167.2,69.3c-86.4,86.4-92.4,224.7-14.9,318 c-7.6,15.3-19.8,33.1-37.9,42c-8.7,4.3-13.6,13.6-12.1,23.2s8.9,17.1,18.5,18.6c4.5,0.7,10.9,1.4,18.7,1.4 c20.9,0,51.7-4.9,83.2-27.6c35.1,18.9,73.5,28.1,111.6,28.1c61.2,0,121.8-23.7,167.4-69.3c44.7-44.7,69.3-104,69.3-167.2 S448.281,114,403.581,69.3z M384.481,384.6c-67.5,67.5-172,80.9-254.2,32.6c-5.4-3.2-12.1-2.2-16.4,2.1c-0.4,0.2-0.8,0.5-1.1,0.8 c-27.1,21-53.7,25.4-71.3,25.4h-0.1c20.3-14.8,33.1-36.8,40.6-53.9c1.2-2.9,1.4-5.9,0.7-8.7c-0.3-2.7-1.4-5.4-3.3-7.6 c-73.2-82.7-69.4-208.7,8.8-286.9c81.7-81.7,214.6-81.7,296.2,0C466.181,170.1,466.181,302.9,384.481,384.6z"></path>
                    <circle cx="236.381" cy="236.5" r="16.6"></circle>
                    <circle cx="321.981" cy="236.5" r="16.6"></circle>
                    <circle cx="150.781" cy="236.5" r="16.6"></circle>
                </g>
            </g> </svg>
    </div>
    <div class="social-network-icons"><a href="https://www.facebook.com/ruyasocial/"
                                         class="social-icon  cursor-customized" target="_blank"
                                         rel="noopener">
            <svg enable-background="new 0 0 56.693 56.693" height="56.693px" version="1.1" viewBox="0 0 56.693 56.693"
                 width="56.693px" xml:space="preserve" xmlns="http://www.w3.org/2000/svg"
                 xlink="http://www.w3.org/1999/xlink"><path
                    d="M40.43,21.739h-7.645v-5.014c0-1.883,1.248-2.322,2.127-2.322c0.877,0,5.395,0,5.395,0V6.125l-7.43-0.029  c-8.248,0-10.125,6.174-10.125,10.125v5.518h-4.77v8.53h4.77c0,10.947,0,24.137,0,24.137h10.033c0,0,0-13.32,0-24.137h6.77  L40.43,21.739z"></path></svg>
        </a>
        <a href="https://twitter.com/ruyasocial/" class="social-icon  cursor-customized"
           target="_blank" rel="noopener">
            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title></title>
                <g>
                    <path d="M23.87,4.43a.5.5,0,0,0-.6-.1,4.76,4.76,0,0,1-.75.27A4.85,4.85,0,0,0,23.37,3a.5.5,0,0,0-.77-.53,10.59,10.59,0,0,1-2.53,1A5.05,5.05,0,0,0,16.5,2a5.71,5.71,0,0,0-3,.93C11.6,4,11.27,6.47,11.41,8a13,13,0,0,1-9-4.76A.53.53,0,0,0,2,3a.5.5,0,0,0-.4.25,5.35,5.35,0,0,0,.22,5.7c-.15-.1-.31-.22-.47-.35A.5.5,0,0,0,.5,9,5.73,5.73,0,0,0,3,13.64l-.39-.11A.5.5,0,0,0,2,14.2a6.48,6.48,0,0,0,4.19,3.62A9.22,9.22,0,0,1,.56,19a.5.5,0,0,0-.31.93A15.2,15.2,0,0,0,8,22H8a13.35,13.35,0,0,0,10-4.63,13.63,13.63,0,0,0,3.65-9.92A9.81,9.81,0,0,0,23.92,5,.5.5,0,0,0,23.87,4.43ZM8,21.5Z"></path>
                </g>
            </svg>
        </a>
        <a href="https://www.instagram.com/ruyaglobal/" class="social-icon  cursor-customized"
           target="_blank" rel="noopener">
            <svg viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><title></title>
                <g>
                    <path d="M17,23H7a6,6,0,0,1-6-6V7A6,6,0,0,1,7,1H17a6,6,0,0,1,6,6V17A6,6,0,0,1,17,23ZM7,3A4,4,0,0,0,3,7V17a4,4,0,0,0,4,4H17a4,4,0,0,0,4-4V7a4,4,0,0,0-4-4Z"></path>
                    <g>
                        <path d="M12,18a6,6,0,1,1,6-6A6,6,0,0,1,12,18ZM12,8a4,4,0,1,0,4,4A4,4,0,0,0,12,8Z"></path>
                    </g>
                    <g>
                        <circle cx="18" cy="5" r="1"></circle>
                    </g>
                </g>
            </svg>
        </a></div>
</div>\
<footer></footer>
</body>
</html>
