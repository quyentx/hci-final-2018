<?php
include_once('includes/connection.php');
include_once('includes/fetch_awards.php');

$award = new Awards();
$awards = $award->fetch_all();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.11/css/all.css"
          integrity="sha384-p2jx59pefphTFIpeqCcISO9MdVfIm4pNnsL08A6v5vaQc4owkQqxMV8kg4Yvhaw/" crossorigin="anonymous">

    <meta name="theme-color" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>RUYA | Web Design &amp; Web Development</title>
</head>
<body class="">

<section class="section-home">
    <div class="popup-menu popup black-background active" style="z-index: 8;">
        <div class="child-element-content">
            <div class="x-button cursor-customized white-color">
                <div class="cross-container" onclick = "window.history.go(-1)">
                    <div class="stick stick1"></div>
                    <div class="stick stick2"></div>
                </div>
            </div>
            <div class="general-container">
                <nav class="menu-container">
                    <ul>
                        <li class="menu-item font-menu-item" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); ">
                            <a class="link-text cursor-customized sound-hover  " href="about-landing.php">about</a>
                        </li>
                        <li class="menu-item font-menu-item" data-cursor-action="menu-link" data-menu="work"
                            data-color="#f2545b" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                            <a class="link-text  cursor-customized sound-hover  " href="work-landing.php">work</a>
                        </li>
                        <li class="menu-item font-menu-item" data-cursor-action="menu-link" data-menu="partners"
                            data-color="#ffc857" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                            <a class="link-text  cursor-customized sound-hover  " href="partners-landing.php">partners</a>
                        </li>
                        <li class="menu-item font-menu-item" data-cursor-action="menu-link" data-menu="awards"
                            data-color="#8c3ba7" style="opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                            <a class="link-text  cursor-customized sound-hover  " href="awards-landing.php">awards</a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</section>

<footer></footer>
</body>
</html>