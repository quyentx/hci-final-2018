
<?php

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="theme-color" content="">
    <title>Work | Ruya Digital</title>
    <link rel="stylesheet" href="style.css">
</head>
<body class="" style="overflow: hidden;">

<section class="section-home" style="display: block;">
    <div class="navigate-circle-button-container "
         style="opacity: 1; pointer-events: auto; transform: matrix(1, 0, 0, 1, -50, 30);">
        <div id="circle_button" class="svg-container" onclick="location.href='../work.php';">
            <svg height="100" width="100">
                <circle stroke="white" stroke-width="3" cx="18" cy="18" r="16" fill="black"></circle>
            </svg>
        </div>
        <div class="navigate-circle-button"><p class="navigate-circle-button-guide menu-font font-color-white blink-slow">
                Press &amp; Hold</p></div>
    </div>
</section>

<?php
include('includes/topbar.php');
?>

<?php
include('includes/bottombar.php');
?>

<div class="main-title-container" style="z-index: 3; display: block;"><p
        class="font-liquido-h1  font-color-white"
        style="top: 0px; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); font-size: 22vw;">Work</p>
</div>
<footer></footer>
</body>
</html>