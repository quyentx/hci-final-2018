<?php
include_once('includes/connection.php');
include_once('includes/fetch_awards.php');

$award = new Awards();
$awards = $award->fetch_all();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.11/css/all.css"
          integrity="sha384-p2jx59pefphTFIpeqCcISO9MdVfIm4pNnsL08A6v5vaQc4owkQqxMV8kg4Yvhaw/" crossorigin="anonymous">

    <meta name="theme-color" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <title>RUYA | Web Design &amp; Web Development</title>
</head>
<body>

<section class="section-home">
    <div id="three-container" class="three-wp" style="opacity: 1;">
        <canvas width="1366" height="635" style="width: 1366px; height: 635px;"></canvas>
    </div>

    <div class="homepage-awards-container" style="opacity: 1; pointer-events: auto;">
        <div class="homepage-awards-container" style="opacity: 1; pointer-events: auto;">
            <?php foreach ($awards as $award) { ?>
                <div class="home-awards-container content-animable"
                     style="transform: matrix(1, 0, 0, 1, 0, 0); opacity: 1;"><p
                            class="menu-font font-color-white bold"><?php echo $award['title']; ?></p> <a
                            href="<?php echo $award['link'] ?>"
                            class="logo-awards social-icon cursor-customized" target="_blank" rel="noopener">
                        <?php echo $award['content'] ?>
                    </a>
                </div>
            <?php } ?>
        </div>
    </div>


</section>

<div class="main-title-container" style="z-index: 3; display: block;"><p
            class="font-liquido-h1 content-animable font-color-white"
            style="top: 0px; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0); font-size: 8vw; text-transform: uppercase">welcome</p></div>
<?php
include('includes/topbar.php');
?>

<?php
include('includes/bottombar.php');
?>

<footer></footer>
</body>
</html>